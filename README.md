# README #


### What is this repository for? ###

This project defines Javascript functions in index.js, which are uploaded to Google Cloud Functions, 
to send a REST request to VictorOps whenever a New/Regressed/Velocity issue is triggered from Firebase Crashlytics

### How do I get set up? ###

1. Install Firebase Tools: npm install -g firebase-tools
2. To make changes, edit index.js in this project
3. To make changes live, run: firebase deploy
4. Check status here: https://console.firebase.google.com/u/0/project/neuroflow-196614/functions/list

Info: Crashlytics specific Cloud Functions docs: https://firebase.google.com/docs/functions/crashlytics-events
