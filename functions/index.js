const functions = require('firebase-functions');
const rp = require('request-promise');

function notifyVictor(issue_id, platform, type) {

  const appName = (platform == 'ios' ? 'ios:com.neuroflowsolution.NeuroFlow' : 'android:com.neuroflow');

  return rp({
    method: 'POST',
    uri: 'https://alert.victorops.com/integrations/generic/20131114/alert/32f7754d-4358-459a-a2db-604c4909bb5d/mobile',
    body: {
      message_type: 'CRITICAL',
      state_message: type + 'https://console.firebase.google.com/u/0/project/neuroflow-196614/crashlytics/app/' + appName + '/issues/' + issue_id,
    },
    json: true,
  });
}

exports.postOnNewIssue = functions.crashlytics.issue().onNew(async (issue) => {
  const issueId = issue.issueId;
  const appPlatform = issue.appInfo.appPlatform;
  await notifyVictor(issueId, appPlatform, 'New Issue: ');
  console.log(`Posted new issue ${issueId} successfully to VictorOps`);
});

exports.postOnRegressedIssue = functions.crashlytics.issue().onRegressed(async (issue) => {
  const issueId = issue.issueId;
  const appPlatform = issue.appInfo.appPlatform;
  await notifyVictor(issueId, appPlatform, 'Regressed Issue: ');
  console.log(`Posted regressed issue ${issueId} successfully to VictorOps`);
});

exports.postOnVelocityAlert = functions.crashlytics.issue().onVelocityAlert(async (issue) => {
  const issueId = issue.issueId;
  const appPlatform = issue.appInfo.appPlatform;
  await notifyVictor(issueId, appPlatform, 'Velocity Alert: ');
  console.log(`Posted velocity alert ${issueId} successfully to VictorOps`);
});